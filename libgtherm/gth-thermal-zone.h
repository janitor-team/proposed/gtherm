/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */
#pragma once

#if !defined (__LIBGTHERM_H_INSIDE__) && !defined (LIBGTHERM_COMPILATION)
#error "Only <libgtherm.h> can be included directly."
#endif

#include "gth-gdbus.h"
#include <glib-object.h>

G_BEGIN_DECLS

#define GTH_TYPE_THERMAL_ZONE (gth_thermal_zone_get_type())

G_DECLARE_FINAL_TYPE (GthThermalZone, gth_thermal_zone, GTH, THERMAL_ZONE, GthGdbusThermalZoneProxy)

const gchar        *gth_thermal_zone_get_path        (GthThermalZone *self);
const gchar        *gth_thermal_zone_get_type_       (GthThermalZone *self);
gint                gth_thermal_zone_get_temperature (GthThermalZone *self);
const gchar *const *gth_thermal_zone_get_cooling_devices_dbus_paths (GthThermalZone *self);


G_END_DECLS
