/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */
#pragma once

#if !defined (__LIBGTHERM_H_INSIDE__) && !defined (LIBGTHERM_COMPILATION)
#error "Only <libgtherm.h> can be included directly."
#endif

#include "gth-gdbus.h"
#include <glib-object.h>

G_BEGIN_DECLS

#define GTH_TYPE_COOLING_DEVICE (gth_cooling_device_get_type())

G_DECLARE_FINAL_TYPE (GthCoolingDevice, gth_cooling_device, GTH, COOLING_DEVICE, GthGdbusCoolingDeviceProxy)

const gchar *gth_cooling_device_get_path          (GthCoolingDevice *self);
const gchar *gth_cooling_device_get_type_         (GthCoolingDevice *self);
gint         gth_cooling_device_get_current_state (GthCoolingDevice *self);
gint         gth_cooling_device_get_max_state     (GthCoolingDevice *self);

G_END_DECLS
