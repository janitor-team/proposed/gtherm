/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */
#pragma once

#include <glib.h>

G_BEGIN_DECLS

#define __LIBGTHERM_H_INSIDE__

#include "gth-cooling-device.h"
#include "gth-manager.h"
#include "gth-names.h"
#include "gth-thermal-zone.h"

G_END_DECLS
