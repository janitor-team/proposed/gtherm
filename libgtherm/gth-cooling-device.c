/*
 * Copyright (C) 2019 Purism SPC
 * SPDX-License-Identifier: LGPL-2.1+
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#include "gth-cooling-device.h"

/**
 * SECTION:gth-cooling-device
 * @short_description: A cooling device
 * @Title: GthCoolingDevice
 *
 * The #GthCoolingDevice represents a cooling device like a fan
 * or a passive cooling mechnism like cpu throttling.
 */

typedef struct _GthCoolingDevice {
  GthGdbusCoolingDeviceProxy parent;

} GthCoolingDevice;

G_DEFINE_TYPE (GthCoolingDevice, gth_cooling_device, GTH_GDBUS_TYPE_COOLING_DEVICE_PROXY);

static void
gth_cooling_device_class_init (GthCoolingDeviceClass *klass)
{
}

static void
gth_cooling_device_init (GthCoolingDevice *self)
{
}

/**
 * gth_cooling_device_get_path:
 * @self: A #GthThermalzone.
 *
 * Gets the DBus path of the #GthCoolingDevice object.
 *
 * Returns: (transfer none): The DBus path of the #GthCoolingDevice object.
 */
const gchar *
gth_cooling_device_get_path (GthCoolingDevice *self)
{
    g_return_val_if_fail (GTH_IS_COOLING_DEVICE (self), NULL);

    return g_dbus_proxy_get_object_path (G_DBUS_PROXY (self));
}

/**
 * gth_cooling_device_get_type_:
 * @self: A #GthThermalzone.
 *
 * Gets the type of the #GthCoolingDevice.
 *
 * Returns: (transfer none): The type of the #GthCoolingDevice.
 */
const gchar *
gth_cooling_device_get_type_ (GthCoolingDevice *self)
{
  g_return_val_if_fail (GTH_IS_COOLING_DEVICE (self), NULL);

  return gth_gdbus_cooling_device_get_type_ (
		 GTH_GDBUS_COOLING_DEVICE (self));
}

/**
 * gth_cooling_device_get_current_state:
 * @self: A #GthThermalzone.
 *
 * Gets the current cooling state of the #GthCoolingDevice.
 *
 * Returns: The #GthCoolingDevice's cooling state.
 */
gint
gth_cooling_device_get_current_state (GthCoolingDevice *self)
{
  g_return_val_if_fail (GTH_IS_COOLING_DEVICE (self), 0);

  return gth_gdbus_cooling_device_get_current_state (
		 GTH_GDBUS_COOLING_DEVICE (self));
}

/**
 * gth_cooling_device_get_max_state:
 * @self: A #GthThermalzone.
 *
 * Gets the maximum cooling state of the #GthCoolingDevice.
 *
 * Returns: The #GthCoolingDevice's maximum cooling state.
 */
gint
gth_cooling_device_get_max_state (GthCoolingDevice *self)
{
  g_return_val_if_fail (GTH_IS_COOLING_DEVICE (self), 0);

  return gth_gdbus_cooling_device_get_max_state (
		 GTH_GDBUS_COOLING_DEVICE (self));
}
