Source: gtherm
Section: x11
Priority: optional
Maintainer: DebianOnMobile Maintainers <debian-on-mobile-maintainers@alioth-lists.debian.net>
Uploaders:
 Henry-Nicolas Tourneur <debian@nilux.be>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-gir,
 gtk-doc-tools,
 libgirepository1.0-dev,
 libglib2.0-dev,
 meson,
 pkg-config,
 valac,
Standards-Version: 4.6.0
Homepage: https://source.puri.sm/Librem5/gtherm
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/DebianOnMobile-team/gtherm.git
Vcs-Browser: https://salsa.debian.org/DebianOnMobile-team/gtherm

Package: gthd
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: DBus service to monitor thermal information
 GThd is a DBus activated daemon that provides information
 about thermal zones, cooling cells and trip points.
 .
 This package contains the daemon.

Package: libgtherm-0.0-0
Architecture: any
Section: libs
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: DBus service to monitor thermal information - shared library
 GThd is a DBus activated daemon that provides information
 about thermal zones, cooling cells and trip points.
 .
 This package contains the shared libraries for applications interfacing with
 Gthd.

Package: libgtherm-dev
Architecture: any
Section: libdevel
Depends:
 gir1.2-gtherm-0.0 (= ${binary:Version}),
 libglib2.0-dev,
 libgtherm-0.0-0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: DBus service to monitor thermal information - development files
 GThd is a DBus activated daemon that provides information
 about thermal zones, cooling cells and trip points.
 .
 This package contains development files and Vala bindings to use when writing
 applications that interface with GThd.

Package: gir1.2-gtherm-0.0
Architecture: any
Section: introspection
Depends:
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Description: GObject introspection data for libgtherm
 GThd is a DBus activated daemon that provides information
 about thermal zones, cooling cells and trip points.
 .
 This package contains introspection data for the libgtherm library.
